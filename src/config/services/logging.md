# Logging

## Syslog

The default installation comes with a syslog daemon, but it is not enabled by
default. To enable it, create a service as documented in this [github page](# dinitctl start <service>)
for the deamon. The command is called syslogd.

### Other syslog daemons

The EvolutionOS repositories also include packages for `rsyslog`, `socklog` and
`metalog`.

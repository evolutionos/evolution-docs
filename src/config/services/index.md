# Services and Daemons - dinit

EvolutionOS uses the dinit supervision suite to
run system services and daemons.

Some advantages of using dinit include:

- really damn fast, much improved upon the previously used runit
- very simple control panel interface
- drunner, a custom made facility to convert runit services to dinit services

If you don't need a program to be running constantly, but would like it to run
at regular intervals, you might like to consider using a [cron
daemon](../cron.md).

## Section Contents

- [Per-User Services](./user-services.md)
- [Logging](./logging.md)

## Service Directories

Each service managed by dinit has an associated *service file*.

Each service file contains various infomation about how to run the service.

For more infomation, visit [the manual](https://github.com/davmac314/dinit)

If a legacy runit service exists, it will be automatically converted on boot

### Configuring Services

For service simplicity and user-friendliness, we recommend you configure your services within the program itself

### Editing Services

To edit a service, first copy its service file to a different name. 
Otherwise, [xbps-install(1)](https://man.voidlinux.org/xbps-install.1) can
overwrite the service file. Then, edit the new service file as needed.
Finally, the old service should be stopped and disabled, and the new one should
be started.

## Managing Services

### Basic Usage

To start, stop, restart or get the status of a service:

```
# dinitctl start <services>
# dinitctl stop <services>
# dinitctl restart <services>
# dinitctl status <services>
```

#### Enabling Services

EvolutionOS provides service files for most daemons in `/etc/dinit.d`.

To enable a service on a system, create a symlink to the service
file in `/etc/dinit.d/boot.d`:

```
# ln -s /etc/dinit.d/<service> /etc/dinit.d/boot.d/
```

You could alternativley use

```
# dinitctl enable <service>
```

Once a service is linked it will always start on boot and restart if it stops,
unless administratively downed.

#### Disabling Services

To disable a service, remove the symlink from the boot.d directory:

```
# rm /etc/dinit.d/boot.d/<service>
```

Or, you could simply run

```
# dinitctl disable <service>
```

#### Testing Services

To check if a service is working correctly when started by the service
supervisor, run it once before fully enabling it:

```
# dinitctl disable <service>
# dinitctl start <service>
```

If everything works, run

```
# dinitctl enable <service>
```
